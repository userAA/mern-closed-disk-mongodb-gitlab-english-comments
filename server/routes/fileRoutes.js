import Router from "express";
const router = new Router();
import authMiddleware from "../middleware/authMiddleware.js";
import {fileController} from "../controllers/index.js";

//router of creating paper
router.post('',authMiddleware,fileController.createDir);
//router of loading files in browser
router.post('/upload',authMiddleware,fileController.uploadFile);
//router of loading avatar user
router.post('/avatar',authMiddleware,fileController.uploadAvatar);
//router of getting files
router.get('',authMiddleware,fileController.getFiles);
//router of loading file on disk
router.get('/download',authMiddleware,fileController.downloadFile);
//router of search file or paper
router.get('/search',authMiddleware,fileController.searchFile);
//router of removing file or paper
router.delete('/',authMiddleware,fileController.deleteFile);
//router of removing avatar user
router.delete('/avatar',authMiddleware,fileController.deleteAvatar);

export default router;