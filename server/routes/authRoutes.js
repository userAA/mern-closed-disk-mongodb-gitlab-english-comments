import Router from "express";
import User from "../models/User.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import {check, validationResult} from "express-validator";
const router = new Router();

import {fileService} from "../services/index.js";
import File from "../models/File.js";

//registration router
router.post("/registration",
    [
        check("email", "Uncorrect email").isEmail(),
        check("password", "Password must be longer than 3 and shorter than 12").isLength({min: 3, max: 12})
    ],
    async (req,res) => {
    try { 
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({message: "Uncorrect request",errors})
        }

        const {email,password} = req.body;
        
        //checking if there is the registered user in the database
        const candidate = await User.findOne({email});
        if (candidate) {
            return res.status(400).json({message: `User with email ${email} already exist`})
        }

        const hashPassword = await bcrypt.hash(password, 5);
        
        //registering new user
        const user = new User({email, password: hashPassword});
        await user.save();

        //creating paper corresponding to the ID of new user
        await fileService.createDir(new File({user: user.id, name: ""}))
        return res.json({message: "User was created"});
    } catch (e) {
        console.log(e);
        res.send({message: "Server error"})
    }
})

//authorization router
router.post("/login", 
    async (req,res) => {
    try { 
        const {email,password} = req.body;

        const user = await User.findOne({email});
        if (!user) {
            return res.status(404).json({message: "User not found"});
        }

        const isPassValid = bcrypt.compareSync(password,user.password);
        if (!isPassValid) {
            return res.status(400).json({message: "Invalid password"});
        }

        //creatштп token of specified router, this token will contain in itself ID of specified user
        const token = jwt.sign({id: user.id}, process.env.secretKey, {expiresIn: "7d"});

        return res.json({
            token,
            user: {
                id: user.id,
                email: user.email,
                diskSpace: user.diskSpace,
                userSpace: user.userSpace,
                avatar: user.avatar
            }
        })
        
    } catch (e) {
        console.log(e);
        res.send({message: "Server error"})
    }
})

export default router;