import fs from "fs";

//function of creating paper in project
export const  createDir = (file) => {
    const filePath = `${process.env.filePath}\\${file.user}\\${file.path}`
        
    return new Promise(((resolve, reject) => {
        try {
            if (!fs.existsSync(filePath)) {
                fs.mkdirSync(filePath)
                return resolve({message: "File was created"})
            } else {
                return reject({message: "File already exist"})
            }
        } catch (e) {
            return reject({message: "File error"})
        }
    }))
}

//the function of removing paper or files from project
export const  deleteFile = (file) => {
    const path = this.getPath(file);
    if (file.type === "dir") {
        fs.rmdirSync(path);
    } else {
        fs.unlinkSync(path)
    }
}

//the function of defining track to paper or file in project
export const  getPath = (file) => {
    return process.env.filePath + '\\' + file.user + '\\' + file.path;
}