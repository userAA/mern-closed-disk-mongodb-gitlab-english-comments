import express from "express";
import mongoose from "mongoose";

import dotenv from "dotenv";
dotenv.config();

import fileUpload from "express-fileupload";
import authRouter from "./routes/authRoutes.js";
import fileRouter from "./routes/fileRoutes.js";
const app = express();
import coreMiddleware from "./middleware/corsMiddleware.js";

app.use(fileUpload({}));
app.use(coreMiddleware);
app.use(express.json());
app.use(express.static("static"));

//including routers of authorization or registration
app.use("/api/auth",authRouter);

//connecting routers for operations with folders and files with an authorized user
app.use("/api/files",fileRouter);

const start = async () => {
    try {        
        //подключаем базу данных Mongo DB
        await mongoose.connect(
        process.env.DB_URL,
        {
            //these are options to ensure that the connection is done properly
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        })
        .then(() => 
        {
            console.log("Successfully connected to MongoDB!")        
        })
        .catch((error) => 
        {
            console.log("Unable to connect to MongoDB!");
            console.error(error);
        }) 
        //слушаем сервер
        app.listen(process.env.PORT || 3001, () => {
            console.log("Server started on port ", process.env.PORT || 3001)
        })
    } 
    catch(error) 
    {
        console.error(error);
    }
}

start()