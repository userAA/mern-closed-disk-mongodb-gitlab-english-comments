gitlab page https://gitlab.com/userAA/mern-closed-disk-mongodb-gitlab-english-comments.git
gitlab comment mern-closed-disk-mongodb-gitlab-english-comments

project mern-closed-disk (storage of files)
technologies used on the frontend:
    axios,
    react,
    react-dom,
    react-redux,
    react-router-dom,
    redux,
    redux-devtools-extension,
    react-transition-group,
    redux-thunk;

used technologies on the backend:
    bcryptjs,
    config,
    express,
    express-fileupload,
    express-validator,
    jsonwebtoken,
    mongoose,
    uuid;

There is user register and user authorization. One can to change user avatar. Each user can have an unlimited number of
add folders, delete folders, add files, delete files in an unlimited number go to the folder and exit it.
The files of every paper one can to sort according to date of creating, name and type.
The file in every current paper one can to find according to name. You can drag and drop files in any distribution. 
