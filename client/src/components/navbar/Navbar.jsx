import React, { useState } from 'react';

import Logo from '../../assets/img/navbar-logo.svg'
import {NavLink} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import {logout} from "../../reducers/userReducer";
import {getFiles, searchFiles } from '../../actions/file';
import { showLoader } from '../../reducers/appReducer';
import avatarLogo from "../../assets/img/avatar.svg";
import {API_URL} from "../../config"

import './navbar.css'

const Navbar = () => {
    //current user authorization flag
    const isAuth = useSelector(state => state.user.isAuth);
    //current paper
    const currentDir = useSelector(state => state.files.currentDir);
    //current user
    const currentUser = useSelector(state => state.user.currentUser);
    
    const dispatch = useDispatch();
    const [searchName, setSearchName] = useState("");
    const [searchTimeout, setSearchTimeout] = useState(false);

    //defining avatar
    const avatar = (
        currentUser !== undefined && 
        currentUser.avatar !== undefined && 
        currentUser.avatar !== null &&
        currentUser.avatar !== '')  ? 
    `${API_URL + currentUser.avatar}` : avatarLogo;

    // the function of search file or paper in current paper according to the entered name
    function searchChangeHandler(e) {
        setSearchName(e.target.value)
        if (searchTimeout !== false) {
            clearTimeout(searchTimeout)
        }
        dispatch(showLoader());
        if (e.target.value !== "") {
            setSearchTimeout(setTimeout((value) => {
                dispatch(searchFiles(value))
            }, 500, e.target.value))
        } else {
            dispatch(getFiles(currentDir))
        }
    }

    return (
        <div className="navbar">
            <div className="container">
                <img src={Logo} alt="" className="navbar__logo"/>
                <div className="navbar__header">MERN CLOUD</div>
                
                {/*search file or paper according to name in current paper*/}
                {isAuth && <input 
                    value={searchName}
                    onChange={e => searchChangeHandler(e)}
                    className="navbar__search" 
                    type="text" 
                    placeholder="search for a file by name..."/>}

                {/*authorizing the user*/}
                {!isAuth && <div className="navbar__login"><NavLink to="/login">Enter</NavLink></div> }

                {/*registering the user*/}
                {!isAuth && <div className="navbar__registration"><NavLink to="/registration">Registration</NavLink></div> }
                
                {/*canceling user authorization*/}
                {isAuth && <div className="navbar__login" onClick={() => dispatch(logout()) }>Escape</div> }
                
                {/*representing user avatar*/}
                {isAuth && <NavLink to="/profile">
                    <img className="navbar__avatar" src={avatar} alt=""/>
                </NavLink>}
            </div>
        </div>
    );
};

export default Navbar;