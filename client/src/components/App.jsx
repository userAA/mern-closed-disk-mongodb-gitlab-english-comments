import React from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import  { useSelector}  from "react-redux";

import Navbar from "./navbar/Navbar";
import Registration from "./authorization/Registration";
import Login from "./authorization/Login";
import Disk from "./disk/disk";

import Profile from "./profile/Profile";

import "./app.css";

function App() {
  //checking user authorization if it equals false then there is no user or user not authorized
  const isAuth = useSelector(state => {return state.user.isAuth;});

  return (
    <BrowserRouter>
      <div className="app">
        {<Navbar/>}
        <div className="wrap">
          {!isAuth ? (
            //user registration or authorization
            <Routes>
              <Route path="/registration" element={<Registration/>} />
              <Route path="/login" element={<Login/>} />
            </Routes>
          )
            :
          (  
            //carring out actions with files and papers on an authorized user
            <Routes>
              <Route path="/" element={<Disk/>} />
              <Route path="/profile" element={<Profile/>}/>
            </Routes>
          )
          }
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
