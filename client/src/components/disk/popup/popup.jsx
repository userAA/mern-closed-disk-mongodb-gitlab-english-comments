import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux";

import { createDir } from "../../../actions/file";
import { setPopupDisplay } from "../../../reducers/fileReducer";
import Input from "../../../utils/input/Input";

import "./popup.css";

const Popup = () => {
    //Name of new paper
    const [dirName, setDirName] = useState(''); 
    const popupDisplay = useSelector(state => state.files.popupDisplay);
    //ID of current paper 
    const currentDir = useSelector(state => state.files.currentDir); 

    const dispatch = useDispatch();

    //creating new paper in current paper
    function createHandler() {
        if (dirName !== "")
        {
            dispatch(createDir(currentDir,dirName));
            setDirName("");
        }
    }

    return (
        <div className="popup" onClick={() => dispatch(setPopupDisplay('none'))} style={{display: popupDisplay}}>
            <div className="popup__content" onClick={(event => event.stopPropagation())}>
                <div className="popup__header">
                    <div className="popup__title">Create new paper</div>
                    {/*closing window of creating paper*/}
                    <button className="popup__close" onClick={() => dispatch(setPopupDisplay('none'))}>X</button>
                </div>
                <Input type="text" placeholder="Enter name of paper..." value={dirName} setValue={setDirName}/>
                 {/*creating paper*/}
                <button className="popup__create" onClick={() => createHandler()}>Create</button>
            </div>
        </div>
    )
}

export default Popup;