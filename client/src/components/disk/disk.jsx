import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import {getFiles, uploadFile} from "../../actions/file";
import FileList from "./fileList/fileList";
import Popup from "./popup/popup";
import { setCurrentDir, setPopupDisplay, setFileView } from "../../reducers/fileReducer";

import UpLoader from "./upLoader/UpLoader";
import { showUploader } from "../../reducers/uploadReducer";

import "./disk.css";

const Disk = () => {
    const dispatch = useDispatch();

    //ID of current directory
    const currentDir = useSelector(state => state.files.currentDir);        
    //flag of file loader in current directory
    const loader = useSelector(state => state.app.loader);                 
    //stack of directories
    const dirStack = useSelector(state => state.files.dirStack);           
    //flag of transmit file
    const [dragEnter, setDragEnter] = useState(false);                     
    //type of sorted files in current directory
    const [sort, setSort] = useState("type");                              
    //flag of showing file loader
    const isUpLoaderVisible = useSelector(state => state.upload.isVisible) 

    useEffect(() => {
        dispatch(getFiles(currentDir,sort));
    }, [dispatch,currentDir,sort]);

    function showPopupHandler() {
        dispatch(setPopupDisplay("flex"));
    }

    function backClickHandler() {
        const backDirId = dirStack.pop();
        dispatch(setCurrentDir(backDirId));
    }

    //file upload function
    function fileUploadHandler(event){
        const files = [...event.target.files];
        if (files.length>0)
        {
            if (isUpLoaderVisible === false) 
            {
                dispatch(showUploader());
            }
            files.forEach(file => dispatch(uploadFile(file, currentDir)));
        }
    }

    //start of the file dragging process
    function dragEnterHandler(event) {
        event.preventDefault();
        event.stopPropagation();
        setDragEnter(true);    
    }

    //end of the file dragging process
    function dragLeaveHandler(event) {
        event.preventDefault();
        event.stopPropagation();
        setDragEnter(false);    
    }

    //the function of adding file at dragging
    function dropHandler(event) { 
        event.preventDefault();
        event.stopPropagation();
        let files = [...event.dataTransfer.files]
        files.forEach(file => dispatch(uploadFile(file,currentDir)))
        setDragEnter(false)
    }

    //while files are being loaded showing loading icon
    if(loader) { 
        return (
            <div className="loader">
                <div className="lds-dual-ring"></div>
            </div>
        )
    }

    return ( !dragEnter ?
        <div className="disk" 
            onDragEnter={dragEnterHandler} 
            onDragLeave={dragLeaveHandler} 
            onDragOver={dragEnterHandler}
        >
            <div className="disk__btns">
                {/*switching between current papers*/}
                <button className="disk__back" onClick={() => backClickHandler()}>Back</button>
                {/*showing window of creating papers*/}
                <button className="disk__create" onClick={() => showPopupHandler()}>Create paper</button>
                {/*Loading of file. When file is being selected, the download window is being displayed. 
                   In this window loader of selected file is being emerged*/}
                <>
                    <label htmlFor="disk__upload-input" className="disk__upload-label">Upload a file</label>
                    <input 
                        multiple={true} 
                        onChange={(event) => fileUploadHandler(event)} 
                        type="file" 
                        id="disk__upload-input" 
                        className="disk__upload-input"
                    />
                </>
                <select 
                    value={sort} 
                    onChange={(e) => {setSort(e.target.value)}} 
                    className="disk__select"
                >
                    <option value="name">On name</option>
                    <option value="type">On type</option>
                    <option value="date">On date</option>
                </select>
                {/* Including grid distribution of files and papers in current paper*/}
                <button className="disk__plate" onClick={() => dispatch(setFileView("plate"))}/>
                {/* Showing of list loaded files and papers in current paper*/}
                <button className="disk__list" onClick={() => dispatch(setFileView("list"))}/>
            </div>
            {/* the list of loaded files and papers in current paper*/}
            <FileList/>
            {/* Window of creating paper in current paper*/}
            <Popup/>
            {/*The window of loading file*/}
            <UpLoader/>
        </div>
        : 
        <div className="drop-area" 
            onDrop={dropHandler} 
            onDragEnter={dragEnterHandler} 
            onDragLeave={dragLeaveHandler}
            onDragOver={dragEnterHandler}
        >
            Drag and drop files here
        </div>
    )
}

export default Disk;