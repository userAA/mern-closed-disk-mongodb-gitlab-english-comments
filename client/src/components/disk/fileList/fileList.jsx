import React from "react";
import { useSelector } from "react-redux";
import {CSSTransition, TransitionGroup} from "react-transition-group";

import File from "./file/file";

import "./fileList.css";
import "../disk.css"

const FileList = () => 
{
    //files in current ppaer
    const files = useSelector(state => state.files.files);  
    //type of file structure
    const fileView = useSelector(state => state.files.view);

    //there are no files in the current folder
    if (files.length === 0) {
        return (
            <div className="loader">
                Files not found
            </div>
        )
    }

    //outputing the grid of files and papers in current paper
    if (fileView === "plate") {
        return (
            <div className="fileplate">
                {files.map(file => 
                    <File key={file._id} file={file}/>
                )}
            </div>
        )
    }

    //outputing the list files and papers in current paper
    if (fileView === "list") {
        return (
            <div className="filelist">
                <div className="filelist__header">
                    <div className="filelist__name">Name</div>
                    <div className="filelist__date">Date</div>
                    <div className="filelist__size">Dimension</div>
                </div>
                <TransitionGroup>
                    {files.map(file => 
                        <CSSTransition 
                            key = {file._id}
                            timeout={500}
                            classNames={'file'}
                            exit = {false}
                        >
                            <File file={file}/>
                        </CSSTransition>
                    )}
                </TransitionGroup>
            </div>
        )
    }
}

export default FileList;