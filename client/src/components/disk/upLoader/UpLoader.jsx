import React from "react";
import { useDispatch, useSelector } from "react-redux";

import UploadFile from "./UploadFile";
import { hideUploader } from "../../../reducers/uploadReducer";

import "./uploader.css"
 
//the window of uploading files
const UpLoader = () => {
    const files = useSelector(state => state.upload.files)
    //the flag of appearing window of uploading files
    const isUpLoaderVisible = useSelector(state => state.upload.isVisible) 
    const dispatch = useDispatch()

    return ( isUpLoaderVisible && 
        <div className="uploader">
            <div className="uploader__header">
                <div className="uploader__title">Loading</div>
                {/*closing window of uploading files*/}
                <button className="uploader__close" onClick={() => dispatch(hideUploader())}>X</button>
            </div>
            {files.map(file =>
                <UploadFile  key = {file} file={file} />
            )}
        </div>
    )
}

export default UpLoader;