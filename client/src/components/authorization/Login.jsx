import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import Input from "../../utils/input/Input"
import { login } from "../../actions/user";

import "./authorization.css";

//authorizing user finding his email and password in data base and defining token user
const Login = () => 
{
    const [email, setEmail] = useState("");  
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();

    const navigate = useNavigate();

    return (
        <div className="authorization">
            <div className="authorization__header">Authorization_</div>
            <Input value={email} setValue={setEmail} type="text" placeholder="Enter email..." />
            <Input value={password} setValue={setPassword} type="password" placeholder="Enter password..." />
            {/*authorization process*/}
            <button className="authorization__btn" onClick={() => dispatch(login(email, password, navigate))}>Enter</button>
        </div>
    )
}

export default Login;