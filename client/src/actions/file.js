import axios from "axios"

import { API_URL } from "../config";
import { hideLoader, showLoader } from "../reducers/appReducer";
import {addFile, deleteFileAction, setFiles} from "../reducers/fileReducer";
import { addUploadFile, changeUploadFile } from "../reducers/uploadReducer";
import { setPopupDisplay } from "../reducers/fileReducer";

//function receiving of files and papers from root directory and sorting them on time data and type
export function getFiles(dirId, sort) { 
    return async dispatch => {
        try {
            //showing loading wheel
            dispatch(showLoader());

            let url = `${API_URL}api/files`;
            if (dirId) {
                url = `${API_URL}api/files?parent=${dirId}`;
            }
            if (sort) {
                url = `${API_URL}api/files?sort=${sort}`;
            }
            if (dirId && sort) {
                url = `${API_URL}api/files?parent=${dirId}&sort=${sort}`;
            }

            //sending to server get request for receiving sorted files and papers in current paper
            const response = await axios.get(url, {
                headers: {Authorization: `Bearer ${localStorage.getItem('token')}`}
            });

            //fixing sorted files
            dispatch(setFiles(response.data));
        } catch (e) {
            alert(e.response.data.message)
        } finally {
            //closing loading wheel
            dispatch(hideLoader())
        }
    }
}

//the function of creating paper
export function createDir(dirId, name) {
    return async dispatch => {
        try {
            const response = await axios.post(`${API_URL}api/files`,{
                name,
                type: 'dir',
                parent: dirId
            }, {
                headers: {Authorization: `Bearer ${localStorage.getItem('token')}`}
            })
            //adding paper in current paper dirId
            dispatch(addFile(response.data));

            //closing the window of creating paper
            dispatch(setPopupDisplay('none'));
        } catch (e) {
            alert(e.response.data.message)
        }
    }
}

//the function of loading file with computer
export function uploadFile(file, dirId) {
    return async dispatch => {
        try {
            const formData = new FormData();
            formData.append('file',file);
            if (dirId) {
                formData.append('parent',dirId);
            }
            //the information about loading file
            const uploadFile = {name: file.name, progress: 0, id: Date.now()}
            //in window of loading files adding the loader of the uploaded file
            dispatch(addUploadFile(uploadFile))

            const response = await axios.post(`${API_URL}api/files/upload`,formData, {
                headers: {Authorization: `Bearer ${localStorage.getItem('token')}`},
                onUploadProgress: progressEvent => {
                    const totalLength = progressEvent.total;        
                    //changing the loader of the uploaded file depending on how the file is being uploaded
                    if (totalLength) {
                        uploadFile.progress = Math.round((progressEvent.loaded*100)/totalLength);
                        dispatch(changeUploadFile(uploadFile))    
                    }
                }
            })
            //fixing uploaded file
            dispatch(addFile(response.data));
        } catch (e) {
            alert(e.response.data.message)
        }
    }
}

//the function of loading file on computer with browser
export async function downloadFile(file) {
    const response = await fetch(`${API_URL}api/files/download?id=${file._id}`,{
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }        
    })
    if (response.status === 200) {
        const blob = await response.blob();
        const downloadUrl = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = downloadUrl;
        link.download = file.name;
        document.body.appendChild(link);
        link.click();
        link.remove();                       
    }
}

//the function of removing file with browser and from server
export function deleteFile(file) {
    return async dispatch => {
        try {
            const response = await axios.delete(`${API_URL}api/files?id=${file._id}`,{
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }                
            })
            dispatch(deleteFileAction(file._id));
            alert(response.data.message)            
        } catch (e) {
            alert(e.response.data.message)
        }
    }
}

//file search function by name
export function searchFiles(search) {
    return async dispatch => {
        try {
            const response = await axios.get(`${API_URL}api/files/search?search=${search}`,{
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }                
            })  
            //fixing found element 
            dispatch(setFiles(response.data));       
        } catch (e) {
            alert(e.response.data.message)
        } finally {
            dispatch(hideLoader())
        }
    }
}