import axios from "axios"

import { API_URL } from "../config";
import {setUser} from "../reducers/userReducer";

//user registration function
export const registration = async (email, password, navigate) => {
    try {
        const response = await axios.post(`${API_URL}api/auth/registration`, {email,password})
        //the user registered successfully
        if (response.data.message === "User was created") 
        {
            // go to the authorization page
            navigate('/login');
        }
    } catch (e) {
        alert(e.response.data.message)
    }
}

//user authorization function
export const login = (email, password, navigate) => {
    return async dispatch => {
        try {
            const response = await axios.post(`${API_URL}api/auth/login`, {email,password})
            //fixing user token
            localStorage.setItem('token',response.data.token);
            //signaling that the user's authorization was successful
            dispatch(setUser(response.data.user)); 
            navigate('/');
        } catch (e) {
            alert(e.response.data.message)
        }
    }
}

//loading user avatar function
export const uploadAvatar = (file) => 
{
    return async dispatch => 
    {
        try 
        {
            const formData = new FormData()
            formData.append('file',file)
            const response = await axios.post(`${API_URL}api/files/avatar`, formData, 
                {headers:{Authorization:`Bearer ${localStorage.getItem('token')}`}}
            )
            //we signal that the avatar has changed
            dispatch(setUser(response.data)); 
        } catch (e) {
            console.log(e)
        }
    }
}

//removing avatar user function
export const deleteAvatar = () => {
    return async dispatch => {
        try {
            const response = await axios.delete(`${API_URL}api/files/avatar`,
                {
                    headers: {Authorization: `Bearer ${localStorage.getItem('token')}`}
                }
            )
            dispatch(setUser(response.data))
        } catch (e) {
            console.log(e)
        }
    }
}